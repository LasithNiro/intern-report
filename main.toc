\select@language {english}
\contentsline {section}{\numberline {1}Company profile}{4}
\contentsline {subsection}{\numberline {1.1}Introduction}{4}
\contentsline {subsection}{\numberline {1.2}Management Structure}{4}
\contentsline {subsection}{\numberline {1.3}Technologies}{4}
\contentsline {subsection}{\numberline {1.4}Products and Services}{4}
\contentsline {subsection}{\numberline {1.5}Technology Partners}{5}
\contentsline {subsection}{\numberline {1.6}Clients}{5}
\contentsline {subsection}{\numberline {1.7}Awards and Publication}{5}
\contentsline {section}{\numberline {2}Training and experience gained}{6}
\contentsline {subsection}{\numberline {2.1}Introduction}{6}
\contentsline {subsection}{\numberline {2.2}Detailed Description about my training in SCoRe Lab}{6}
\contentsline {subsection}{\numberline {2.3}My Contribution}{6}
\contentsline {subsubsection}{\numberline {2.3.1}Research about "Detecting Dengue breading sites using aerial images."}{6}
\contentsline {paragraph}{Image processing approach}{6}
\contentsline {paragraph}{Manual approach}{6}
\contentsline {subsubsection}{\numberline {2.3.2}elephant localization using infra sound data}{6}
\contentsline {subsubsection}{\numberline {2.3.3}Drone tracking application for \ac {NTU}, Singapore }{6}
\contentsline {subsubsection}{\numberline {2.3.4}Web server for elephant identification using aerial images.}{6}
\contentsline {subsubsection}{\numberline {2.3.5}Working as a mentor of Google code-in}{6}
\contentsline {section}{\numberline {3}Observation and Comments}{7}
\contentsline {subsection}{\numberline {3.1}Self review}{7}
\contentsline {section}{\numberline {4}Reflection}{8}
\contentsline {subsection}{\numberline {4.1}Reflection \& Learning Outcome}{8}
\contentsline {subsection}{\numberline {4.2}Conclusion}{8}
\contentsline {section}{\numberline {5}Appendices}{9}
\contentsline {section}{\numberline {6}References}{10}
\contentsline {section}{\numberline {7}Progress reports}{11}
\contentsline {section}{\numberline {8}Letter of Completion}{11}
\contentsline {section}{\numberline {9}Industrial Training Programme Evaluation Form}{11}
